import { Router } from 'express';

import { ListsFactory } from './lists/ListsFactory';
import { StoreController } from './lists/StoreController';
import { OfferController } from './offers/OfferController';
import { OfferFactory } from './offers/OfferFactory';

export default class Routes {
  private offerController: OfferController = OfferFactory.offerController();
  private storeController: StoreController = ListsFactory.storeController();

  public setupRoutes(router: Router) {
    router.get(
      '/api/stores',
      this.storeController.getStores.bind(this.storeController)
    );
    router.get(
      '/api/offers/:storeId',
      this.offerController.getOffers.bind(this.offerController)
    );
  }
}
