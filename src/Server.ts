import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import * as helmet from 'helmet';
import mongoose = require('mongoose');
import * as path from 'path';

import Routes from './Routes';

export class Server {
  public static bootstrap(): Server {
    return new Server();
  }

  public app: express.Application;
  public routes: Routes = new Routes();

  constructor() {
    this.app = express();
    this.setupMiddlewares();
    this.setupRoutes();
    this.setupDbConnection();
    this.boot();
  }

  public setupMiddlewares() {
    this.app.use(helmet());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());

    const env = process.env.NODE_ENV;
    const corsConfig = { origin: env === 'prod' ? 'https://localhost/' : '*' };
    this.app.use(cors(corsConfig));
  }

  // Setup router
  private setupRoutes() {
    let router: express.Router;
    router = express.Router();
    this.routes.setupRoutes(router);
    this.app.use(router);
  }

  // Setup DB connection
  private setupDbConnection() {
    const CONNECTION_URL = 'mongodb://127.0.0.1:27017/bonzai';
    mongoose.connect(CONNECTION_URL);
    mongoose.Promise = global.Promise;
    const db = mongoose.connection;

    db.on('error', console.error.bind(console, 'MongoDB connection error:'));
  }

  private boot() {
    this.app.listen(3005, () => {
      console.info('Application successfully started on port [3005]');
    });
  }
}

Server.bootstrap();
