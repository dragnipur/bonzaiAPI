import { Response } from 'express';
import { v1 as uuid } from 'uuid';

import { IStore, Store } from './model/Store';

export class StoreController {
  public async getStores(req: Request, res: Response) {
    const stores: IStore[] = await Store.find();
    res.status(200).send(stores);
  }
}
