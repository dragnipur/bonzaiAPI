import { Document, Model, model, Schema } from "mongoose";

export interface IStore extends Document {
    id: string;
    title: string;
    label: string;
}

export const StoreSchema: Schema = new Schema({
    _id: String,
    title: String,
    label: String,
});

export const Store: Model<IStore> = model<IStore>("Store", StoreSchema);