import { AhOfferService } from './AhOfferService';
import { IOfferService } from './IOfferService';
import { IOffer } from './Offer';
import { OfferController } from './OfferController';

export class OfferFactory {
  private static iOfferController: OfferController;
  private static iOfferServices: Map<string, IOfferService>;

  public static offerController(): OfferController {
    if (!OfferFactory.iOfferController) {
      OfferFactory.iOfferController = new OfferController();
    }
    return OfferFactory.iOfferController;
  }

  public static offerServices(): Map<string, IOfferService> {
    if (!OfferFactory.iOfferServices) {
      const services = new Map<string, IOfferService>();
      services.set('AH', new AhOfferService());
      OfferFactory.iOfferServices = services;
    }
    return OfferFactory.iOfferServices;
  }
}
