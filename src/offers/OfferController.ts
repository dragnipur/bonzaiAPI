import { Response, Request } from 'express';
import * as cron from 'node-cron';

import { AhOfferService } from './AhOfferService';
import { IOfferService } from './IOfferService';
import { IOffer, Offer, IOfferModel } from './Offer';
import { Store } from '../lists/model/Store';
import { OfferFactory } from './OfferFactory';

export class OfferController {
  constructor() {
    cron.schedule('0 1 * * *', this.fetchOffers.bind(this), true);
    this.fetchOffers();
  }

  private async fetchOffers() {
    const stores = await Store.find();
    const offerServices = OfferFactory.offerServices();
    await Offer.remove({});

    stores.forEach(store => {
      if (offerServices.has(store.label)) {
        offerServices.get(store.label).fetchOffers(store._id);
      }
    });
  }

  public async getOffers(req: Request, res: Response) {
    try {
      const storeId = req.params.storeId;
      const offers: IOfferModel[] = await Offer.find({ store: storeId });
      res.status(200).send(offers);
    } catch (err) {
      res.status(400).send();
    }
  }
}
