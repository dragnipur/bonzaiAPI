import { Document, Model, model, Schema } from 'mongoose';

export interface IOffer {
  title: string;
  store: Schema.Types.ObjectId;
  discountLabel: string;
  oldPrice: number;
  newPrice: number;
}

export interface IOfferModel extends Document {}

export const OfferSchema: Schema = new Schema({
  title: String,
  store: { type: Schema.Types.ObjectId, ref: 'Store' },
  discountLabel: String,
  oldPrice: Number,
  newPrice: Number
});

export const Offer: Model<IOfferModel> = model<IOfferModel>(
  'Offer',
  OfferSchema
);
