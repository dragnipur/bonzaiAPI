import axios from 'axios';

import { IOfferService } from './IOfferService';
import { IOffer, Offer } from './Offer';
import { Schema, Mongoose, Types } from 'mongoose';

export class AhOfferService implements IOfferService {
  public async fetchOffers(storeId) {
    const ahBonusResponse = await axios(
      'https://www.ah.nl/service/rest/delegate?url=%2Fbonus'
    );
    const data = ahBonusResponse.data;
    let offers: IOffer[] = [];

    const lanes = data._embedded.lanes;

    lanes.filter(this.isProductLane).forEach(lane => {
      const items = lane._embedded.items;

      const products: IOffer[] = items
        .filter(this.isDiscountedProduct)
        .map(laneItem => this.laneItemToIOffer(laneItem, storeId));

      offers = offers.concat(products);
    });

    console.log('save');
    offers.forEach(offer => new Offer(offer).save());
  }

  private isProductLane(lane) {
    return lane.type === 'ProductLane';
  }

  private isDiscountedProduct(laneItem) {
    return (
      laneItem.resourceType === 'Product' &&
      laneItem._embedded.product.priceLabel
    );
  }

  private laneItemToIOffer(laneItem, storeId: Schema.Types.ObjectId): IOffer {
    const product = laneItem._embedded.product;
    return {
      title: product.description,
      store: storeId,
      discountLabel: product.discount.label,
      oldPrice: product.priceLabel.was,
      newPrice: product.priceLabel.now
    };
  }
}
