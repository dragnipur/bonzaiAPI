import { IOffer } from './Offer';
import { Schema } from 'mongoose';

export interface IOfferService {
  fetchOffers(id: Schema.Types.ObjectId);
}
